Sysinfo() {
	IP_Address=$(ifconfig -a | grep inet | grep -v 127.0.0.1 | grep -v inet6 | awk '{print $2}' | tr -d "addr:" | awk 'NR==1')
	Startup=$(awk '{a=$1/86400;b=($1%86400)/3600;c=($1%3600)/60} {printf("%d 天 %d 小时 %d 分钟\n",a,b,c)}' /proc/uptime)
	echo -e "\n"
	echo "           内核版本:		$(uname -rs)"
	echo "           IP 地址 : 		${IP_Address}"
	echo "           运行时间: 		${Startup}"
	echo ""
}

[ -e /tmp/.failsafe ] && export FAILSAFE=1
clear
[ -f /etc/banner ] && cat /etc/banner
[ -n "$FAILSAFE" ] && cat /etc/banner.failsafe

export PATH="/usr/sbin:/usr/bin:/sbin:/bin"
export HOME=$(grep -e "^${USER:-root}:" /etc/passwd | cut -d ":" -f 6)
export HOME=${HOME:-/root}
export PS1='\u@\h:\w\$ '
export ENV=/etc/shinit

case "$TERM" in
	xterm*|rxvt*)
		export PS1='\[\e]0;\u@\h: \w\a\]'$PS1
		;;
esac

[ -n "$FAILSAFE" ] || {
	for FILE in /etc/profile.d/*.sh
	do
		[ -e "$FILE" ] && . "$FILE"
	done
	unset FILE
}

Sysinfo
alias coremarkd='sh /etc/coremark.sh'
alias shutdown='sync && poweroff'
